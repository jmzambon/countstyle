
Syntaxe :

COMPTE.STYLE(plage, style [, fonction])
- <plage> : les cellules de référence
- <style> : le style à compter (sensible à la casse)
- <fonction> : un entier représentant l'opération à appliquer
       0 ou paramètre absent : nombres de cellules
       1 : somme des valeurs
       2 : nombre de cellules non vides
       3 : moyenne des valeurs
       4 : valeur maximale
       5 : valeur minimale
       6 : nombre de valeurs numériques

STYLE.CELLULE(cellule)
- <cellule> : la cellule dont on souhaite connaître le style

