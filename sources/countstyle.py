# -*- coding: utf-8 -*-

import unohelper
from org.countstyle import XCountStyle
from com.sun.star.util import XModifyListener
from com.sun.star.uno import RuntimeException
from com.sun.star.lang import IllegalArgumentException
from com.sun.star.sheet.GeneralFunction import (SUM, COUNT, AVERAGE, MAX, MIN, COUNTNUMS)


styles = {}
styles_rev = {}
components = {}
colortable = {}


class ModifyListener(unohelper.Base, XModifyListener):
    def disposing(self, event):
        try:
            docname = event.Source.Namespace
            del components[docname]
        except Exception:
            pass

    def modified(self, event):
        event.Source.calculateAll()


class CountStyle(unohelper.Base, XCountStyle):
    def __init__(self, ctx):
        self.ctx = ctx
        # for functions available with XSheetOperation see
        # https://www.openoffice.org/api/docs/common/ref/com/sun/star/sheet/GeneralFunction.html
        # for issues with these functions, see https://bz.apache.org/ooo/show_bug.cgi?id=22625
        self.functions = {1: SUM, 2: COUNT, 3: AVERAGE, 4: MAX, 5: MIN, 6: COUNTNUMS}
        self._getcolortable()

    # XCountStyle
    def countstyle(self, doc, cellrange, reference, function):
        if function == 0:
            function = None
        if not reference:
            raise IllegalArgumentException(u'Second argument is incorrect.', self, 2)
        if function and function not in self.functions:
            raise IllegalArgumentException(u'Third argument is incorrect.', self, 3)
        if doc.Namespace not in components:
            components[doc.Namespace] = ModifyListener()
            doc.addModifyListener(components[doc.Namespace])
        # getting internal styles once per session
        if not styles:
            self._getinternalstyles(doc)
        try:    # is refernce an *internal* style name?
            reference = styles[reference]
        except KeyError:
            pass
        # count reference formatted cells
        formatranges = cellrange.UniqueCellFormatRanges
        if not function:
            cellcount = 0
            for n in range(formatranges.Count):
                frs = formatranges.getByIndex(n)
                if frs.getPropertyValue("CellStyle") == reference:
                    for m in range(frs.Count):
                        fr = frs.getByIndex(m)
                        cellcount += len(fr.Data)*len(fr.Data[0])
            value = cellcount
        # compute function on reference
        else:
            sheetcellranges = doc.createInstance("com.sun.star.sheet.SheetCellRanges")
            for n in range(formatranges.Count):
                frs = formatranges.getByIndex(n)
                if frs.getPropertyValue("CellStyle") == reference:
                    sheetcellranges.addRangeAddresses(frs.RangeAddresses, True)
            if sheetcellranges.Count:
                try:
                    value = sheetcellranges.computeFunction(self.functions[function])
                except RuntimeException:
                    value = 0
            else:
                value = None
        return value

    def cellstyle(self, doc, cell):
        # cell range not allowed here, let's be explicit
        if cell.Rows.Count > 1 or cell.Columns.Count > 1:
            raise IllegalArgumentException
        if doc.Namespace not in components:
            components[doc.Namespace] = ModifyListener()
            doc.addModifyListener(components[doc.Namespace])
        # getting internal styles once per session
        if not styles:
            self._getinternalstyles(doc)
        try:
            return styles_rev[cell.CellStyle]
        except KeyError:
            return cell.CellStyle

    # private methods
    def _getcolortable(self):
        ColorTable = self.ctx.ServiceManager.createInstance("com.sun.star.drawing.ColorTable")
        for colorname in ColorTable.ElementNames:
            colortable[colorname] = ColorTable.getByName(colorname)

    def _getinternalstyles(self, doc):
        cellstyles = doc.StyleFamilies.CellStyles
        for n in range(cellstyles.Count):
            style = cellstyles.getByIndex(n)
            if style.DisplayName != style.Name:
                styles[style.DisplayName] = style.Name
                styles_rev[style.Name] = style.DisplayName


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(CountStyle, "org.countstyle", ())
