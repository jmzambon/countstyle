# COUNTSTYLE
COUNTSTYLE is an extension dedicated to the spreadsheet module of Apache OpenOffice and LibreOffice suites.<br/>
It is aimed to perform operations based on the cell formatting, such as typically (but not only) the background colour.

![countstyle](img/countstyle example.png)


## Instructions
Download the most recent `countstyle-x.x.x.oxt` file and install it from *Tools->Extension Manager*.<br>Restart the program.

The extension adds two new functions to the spreadsheet module, accessible from the function wizard: COUNTSTYLE and CELLSTYLE.

Formulas using these functions are automatically updated when a new style is applied.
##### Syntax
    COUNTSTYLE(range, style [, function])
\<range\>: the referred cell range<br/>
\<style\>: the style to count (case sensitive)<br/>
\<function\>: an integer giving the operation to compute<br/>
&nbsp;&nbsp;&nbsp;&nbsp;0 or missing argument: number of cells<br/>
&nbsp;&nbsp;&nbsp;&nbsp;1: sum of values<br/>
&nbsp;&nbsp;&nbsp;&nbsp;2: number of non empty cells<br/>
&nbsp;&nbsp;&nbsp;&nbsp;3: average of values<br/>
&nbsp;&nbsp;&nbsp;&nbsp;4: maximal value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;5: minimal value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;6: number of numeric values

    CELLSTYLE(cell)
\<cell\>: the cell from which you want to get the style
##### Tips
As Jeff pointed it out on the [french forum](https://forum.openoffice.org/fr/forum/viewtopic.php?p=332571#p332571), it is also possible to count multiple styles at once:
![multicount](img/multicount.png)


## Limitations
1. By design, COUNTSTYLE only counts cells formatted with the style given as argument. Direct formatting is ignored and colour names are not allowed.
2. COUNTSTYLE doesn't work with style applied by mean of conditional formatting: not only would this be awkward and unwiedly (there is no easy API method to retrieve such colouring), but also useless, as you just need to count cells on the same condition used for formatting.

## Why counting styles rather than colours?
It's far easier for the user to define a style rather that trying to retrieve a colour reference.<br/>
And what should this reference be: the colour name?, the decimal value?, the rgb or the hexadecimal representation?

By the way, what colour should COUNTSTYLE count: the background colour?, the characters colour?, the borders one?...

Colour are not the same for everyone, and besides, colour names and values differ between OpenOffice and LibreOffice. For example, OpenOffice "red" is rgb 255,51,51, while one would expect 255,0,0!

With styles you can count coloured cells, but you can also count on any property you want: background or character colour, character weight, cell borders, font, any mix of those and so on...